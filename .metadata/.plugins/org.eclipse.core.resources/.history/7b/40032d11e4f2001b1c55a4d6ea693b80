
import java.util.Map;
import java.util.Stack;

public class MassCalculator {
	
	private Map<Character, Integer> molecularMassOfAtoms;
	private final int OPEN_BRACKET_CODE = -1;
	
	public MassCalculator() {
		molecularMassOfAtoms.put('O', 16);
		molecularMassOfAtoms.put('C', 12);
		molecularMassOfAtoms.put('H', 1);
	}
	
	/**
	 * Calculates the molecular mass of the given formula
	 * @param formula
	 * @return molecular mass
	 */
	public int calculateMass(String formula) {
		if (formula == null)
			throw new AssertionError("Formula can't be null.");
		
		Stack<Integer> stack = new Stack<>();
		int mass=0, sum=0;
		
		for (Character ch: formula.toUpperCase().toCharArray()) {
			if (ch == ')') {
				sum = 0;
				while (stack.peek() != OPEN_BRACKET_CODE) {
					sum += stack.pop();
				}
				stack.pop();
				stack.push(sum);
			} else if (isNumber(ch)) {
				sum = stack.pop() * Integer.parseInt(ch.toString());
				stack.push(sum);
			} else
				stack.push(molecularMassOfAtoms.get(ch));
		}
		
		while (!stack.isEmpty())
			mass += stack.pop();
		
		return mass;
	}
	
	/**
	 * Private helper function of calculateMass() to get value of 
	 * @param ch
	 * @return
	 */
	private Integer getValue(char ch) {
		int result = 0;
		switch(ch) {
		case 'C':
			result = 12;
			break;
		case 'O':
			result = 16;
			break;
		case 'H':
			result = 1;
			break;
		return result;
	}
	
	private boolean isNumber(char ch) {
		return ch >='0' && ch <='9';
	}
}
