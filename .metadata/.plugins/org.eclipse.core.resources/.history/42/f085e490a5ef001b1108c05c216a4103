
/**
 * Expression Evaluator class works on arithmetic operators (+, -, *, /),
 * relational operators (==, !=, <, >, <=, >=),
 * and boolean operators (&&, ||, !), and parentheses.
 * assumes the expression has only integer constants, and the tokens are separated using spaces
 */
public class ExpressionEvaluator {
	private String infixExpression;
    private Stack<String> operatorStack;

    public ExpressionEvaluator(String infixExpression) {
        this.infixExpression = infixExpression;
    }

    public String evaluate() {
        String postfixExpression = infixToPostfix();
        Stack<String> stack = new Stack<>(postfixExpression.length()/2+1);
        
        for (String s: postfixExpression.split(" ")) {
            if (isValidOperator(s) && !isBooleanOperator(s)) {
                String operand1 = stack.pop();
                String operand2 = stack.pop();
                stack.push(execute(operand1, operand2, s));
            }else if( "!".equals(s)) {
            	 String operand1 = stack.pop();
                 stack.push(execute(operand1,null, s));
            }else {
                stack.push(s);
            }
        }
        return String.valueOf(stack.pop());
    }

    private String execute(String operand1, String operand2, String operator) {
        String result=null;
		switch (operator) {
		case "+":
			result = String.valueOf(Double.parseDouble(operand1) + Double.parseDouble(operand2));
			break;
		case "-":
			result = String.valueOf(Double.parseDouble(operand1) - Double.parseDouble(operand2));
			break;
		case "*":
			result = String.valueOf(Double.parseDouble(operand1) * Double.parseDouble(operand2));
			break;
		case "/":
			result = String.valueOf(Double.parseDouble(operand1) / Double.parseDouble(operand2));
			break;
		case ">":
			result = String.valueOf(Double.parseDouble(operand1) > Double.parseDouble(operand2));
			break;
		case "<":
			result = String.valueOf(Double.parseDouble(operand1) < Double.parseDouble(operand2));
			break;
		case ">=":
			result = String.valueOf(Double.parseDouble(operand1) >= Double.parseDouble(operand2));
			break;
		case "<=":
			result = String.valueOf(Double.parseDouble(operand1) <= Double.parseDouble(operand2));
			break;
		case "==":
			result = String.valueOf(Double.parseDouble(operand1) == Double.parseDouble(operand2));
            break;
		case "!=":
			result = String.valueOf(Double.parseDouble(operand1) != Double.parseDouble(operand2));
            break;
		case "&&":
			result = String.valueOf(Boolean.parseBoolean(operand1) && Boolean.parseBoolean(operand2));
            break;
        case "!":	
        	result = String.valueOf(!Boolean.parseBoolean(operand1));
            break;
        case "||":
        	result = String.valueOf(Boolean.parseBoolean(operand1) || Boolean.parseBoolean(operand2));
            break;
		}
        return result;
    }

    String infixToPostfix() {
        StringBuilder postfixExpression = new StringBuilder();
        operatorStack = new Stack<>(infixExpression.length());
        
        for(String ch: infixExpression.split(" ")) {
            // An operand is scanned, push it
        	 if(!isValidOperator(ch)) {
        		postfixExpression.append(ch + " ");
        	 }else if ("(".equals(ch)) {
                operatorStack.push(ch);
             }else if (")".equals(ch)) {
                while (!operatorStack.isEmpty() && !"(".equals(operatorStack.peek())) {
                    postfixExpression.append(operatorStack.pop() + " ");
                }
                operatorStack.pop();
             }else {
        		while (!operatorStack.isEmpty() && getPrecedence(ch) <= getPrecedence(operatorStack.peek())) {
                    postfixExpression.append(operatorStack.pop() + " ");
                }
                operatorStack.push(ch);
        	 }
        }
        while (!operatorStack.isEmpty())
            postfixExpression.append(operatorStack.pop() + " ");

        return postfixExpression.toString().trim();
    }

    private boolean isValidOperator(String op) {
        String operators = "+-*/)(";
        return operators.contains(op) || isRelationalOperator(op) || isBooleanOperator(op);
    }

    private int getPrecedence(String op) {
        int result = -1;
 
        switch (op) {
            case "/":
            case "*":
                result = 5;
                break;
            case "+":
            case "-": 
                result = 4;
                break; 
            case ">":
            case "<":
            case ">=":
            case "<=":
            case "!=":
            	 result = 3;
                 break;
            case "==":	
            case "!":	
            	result = 2;
                break;
            case "&&":
            case "||":
            	 result = 1;
                 break;
        }
        return result;
    }
    
    private boolean isRelationalOperator(String op) {
        switch(op) {
            case ">":
            case "<":
            case ">=":
            case "<=":
            case "==":
            case "!=":
                return true;
        }
        return false;
    }
    private boolean isBooleanOperator(String op) {
        switch(op) {
            case "&&":
            case "!":
            case "||":
                return true;
        }
        return false;
    }
}
