import java.util.ArrayList;
import java.util.List;

public class Heap {

	private List<Integer> heap;

	public Heap() {
		heap = new ArrayList<>();
	}

	public void insert(int element) {
		heap.add(element);
		if (heap.size() > 1) {
			for (int k = heap.size() / 2 - 1; k >= 0; k--) {
				heapify(k);
			}
		}
	}

	private void heapify(int k) {
		int n = heap.size();
		int largest = k;
		int left = 2 * k + 1;
		int right = 2 * k + 2;
		
		if (left < n && heap.get(left) > heap.get(largest))
			largest = left;
		if (right < n && heap.get(right) > heap.get(largest))
			largest = right;

		if (largest != k) {
			// Swapping
			int temp = heap.get(largest);
			heap.set(largest, heap.get(k));
			heap.set(k, temp);

			heapify(largest);
		}
	}

	public int extractMax() {
		if (isEmpty())
			throw new AssertionError("Heap is empty");

		int element = heap.get(0);
		heap.set(0, heap.get(heap.size() - 1)); // Swap element to remove with last element
		
		heap.remove(heap.size() - 1);

		for (int k = heap.size() / 2 - 1; k >= 0; k--) {
			heapify(k);
		}

		return element;
	}

	public void printHeap() {
		for (int element : heap) {
			System.out.print(element + "\t");
		}
	}

	public boolean isEmpty() {
		return (heap.size() == 0);
	}
}
