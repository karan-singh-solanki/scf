import java.util.ArrayList;
import java.util.List;

public class Heap {

	private List<Bowler> heap;

	public Heap() {
		heap = new ArrayList<Bowler>();
	}

	public void insert(Bowler bowler) {
		heap.add(bowler);
		if (heap.size() > 1) {
			for (int k = heap.size() / 2 - 1; k >= 0; k--) {
				heapify(k);
			}
		}
	}

	private void heapify(int k) {
		int n = heap.size();
		int largest = k;
		int left = 2 * k + 1;
		int right = 2 * k + 2;
		
		if (left < n && heap.get(right).compareTo(heap.get(largest)) == 1)
			largest = left;
		if (right < n && heap.get(right).compareTo(heap.get(largest)) == 1)
			largest = right;

		if (largest != k) {
			// Swapping
			Bowler bowler = heap.get(largest);
			heap.set(largest, heap.get(k));
			heap.set(k, bowler);

			heapify(largest);
		}
	}

	public Bowler extractMax() {
		if (isEmpty())
			throw new AssertionError("Heap is empty");

		Bowler bowler = heap.get(0);
		heap.set(0, heap.get(heap.size() - 1)); // Swap element to remove with last element
		heap.remove(heap.size() - 1);

		for (int k = heap.size() / 2 - 1; k >= 0; k--) {
			heapify(k);
		}

		return bowler;
	}

	public void printHeap() {
		for (int element : heap) {
			System.out.print(element + "\t");
		}
	}

	public boolean isEmpty() {
		return (heap.size() == 0);
	}
}
