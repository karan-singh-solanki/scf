
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

public class MassCalculator {
	
	private Map<String, Integer> moleculeMassList;
	
	public MassCalculator() {
		moleculeMassList = new LinkedHashMap<>();
		moleculeMassList.put("C",12);
		moleculeMassList.put("O",16);
		moleculeMassList.put("H",1);
	}
	
	public int calculateMass(String formula) {
		if (formula == null)
			throw new AssertionError("Formula can't be null.");
		
		Stack<Integer> stack = new Stack<>();
		int mass=0, sum=0;
		
		for (Character ch: formula.toUpperCase().toCharArray()) {
			if (ch == ')') {
				sum = 0;
				while (stack.peek() != -1) {
					sum += stack.pop();
				}
				stack.pop();
				stack.push(sum);
			} else if (isNumber(ch)) {
				sum = stack.pop() * Integer.parseInt(ch.toString());
				stack.push(sum);
			} else
				stack.push(getValue(ch));
		}
		
		while (!stack.isEmpty())
			mass += stack.pop();
		
		return mass;
	}
	
	private Integer getValue(char ch) {
		int result = 0;
		switch(ch) {
		case 'C':
			result = 12;
			break;
		case 'O':
			result = 16;
			break;
		case 'H':
			result = 1;
			break;
		case '(':
			result = -1;
			break;
		case ')':
			result = -2;
			break;
		}
		return result;
	}
	
	private boolean isNumber(char ch) {
		return ch >='0' && ch <='9';
	}
	
	public static void main(String args[]) {
		MassCalculator massCalculator = new MassCalculator();
		System.out.println(massCalculator.calculateMass("CH3(OH)2"));
	}
}
